export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    htmlAttrs: {
      lang: 'en'
    },
    title: 'Phoenix Arts - Realm of high quality software',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { property: 'og:title', content: 'Phoenix Arts - Realm of high quality software'},
      { property: 'og:type', content: 'website'},
      { property: 'og:image', content: 'https://phoenixarts-kohl.vercel.app/bg/laptop.jpg'},
      { property: 'og:description', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras in dui sapien. Aliquam rhoncus ipsum ullamcorper erat gravida bibendum. Mauris egestas eros non vestibulum mollis. Ut tempor erat velit, vel placerat arcu gravida vel.'}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/phoenix-logo.png' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    'vuesax/dist/vuesax.css',
    'boxicons/css/boxicons.css'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '@/plugins/vuesax'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  }
}
