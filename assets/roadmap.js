(() => {

function getTitles(steps) {
    let map = []
    for (const step of steps) {
        if (step.className == 'data') {
            map.push({
                kind: step.className,
                text: step.innerHTML
            })
        }
    }
    // Cleanup
    for (const step of [...steps]) {
        if (step.className == 'data') {
            step.remove()
        }
    }
    return map
}

function getLogo(steps) {
    let logo = null
    for (const step of steps) {
        if (['graphics'].includes(step.className)) {
            logo = step
        }
    }
    // Cleanup
    for (const step of steps) {
        if (['graphics'].includes(step.className)) {
            step.style.display = 'none'
        }
    }
    return logo
}

function main() {
    const roadmaps = document.querySelectorAll('#roadmap')
    for (const roadmap of roadmaps) {
        const value = roadmap.getAttribute('value') 
        const elements = getTitles(roadmap.children)
        function draw(params) {
            const canvas = document.createElement('canvas')
            const canvas2 = document.createElement('canvas')
            const ctx = canvas.getContext('2d')
            const ctx2 = canvas2.getContext('2d')
            const gap = 200
            const max = elements.length * gap
            const val = value * gap
            const res = 0.5
            const logo = getLogo(roadmap.children)
            const isMobile = window.innerWidth <= 1200
            const scale = window.devicePixelRatio
            roadmap.style.position = 'relative'
            canvas.style.display = 'block'
            canvas.style.margin = '0 auto'
            canvas.height = Math.floor((max + gap) * scale)
            canvas.style.height = `${max + gap}px`
            ctx.scale(scale, scale)
            if (isMobile) {
                canvas.width = Math.floor(window.innerWidth * scale)
                canvas.style.width = `${window.innerWidth}px`
            }
            else {
                canvas.width = Math.floor(window.innerWidth/2 * scale)
                canvas.style.width = `${window.innerWidth/2}px`
            }
            canvas2.style.position = 'absolute'
            canvas2.style.top = '0'
            canvas2.height = Math.floor((max + gap) * scale)
            canvas2.style.width = `${window.innerWidth}px`
            canvas2.style.height = `${max + gap}px`
            ctx2.scale(scale, scale)
            if (isMobile) {
                canvas2.width = Math.floor(window.innerWidth * scale)
                canvas2.style.width = `${window.innerWidth}px`
            }
            else {
                canvas2.width = Math.floor(window.innerWidth/2 * scale)
                canvas2.style.width = `${window.innerWidth/2}px`
            }
            roadmap.style.height = `${(max + gap)/scale}px`
            roadmap.style.overflowY = 'hidden'
            if (!value) return console.error('#roadmap needs \'value\' attribute to show the state')
            if (!logo) return console.error('#roadmap needs \'logo\' element to show the logo')
            roadmap.appendChild(canvas)
            roadmap.appendChild(canvas2)
            const fx = (i) => (50+i/max*canvas.width/((isMobile) ? 5 : 4)) * Math.cos((Math.PI/gap)*i) + canvas.width/2
            // Grey road
            for(let i = 0; i < max; i += res) {
                ctx.beginPath()
                ctx.fillStyle = `rgb(${i/max*50+50},${i/max*50+50},${i/max*50+50})`
                ctx.arc(
                    fx(i), 
                    i, 
                    i/max*50, 
                    0, Math.PI*2
                )
                ctx.fill()
            }
            // Discovered road
            let i = 0
            let chunk = 1
            function discover() {
                // Chunk loop
                // console.log(i*gap/max)
                if (chunk < 20) chunk *= 1.1
                // chunk = Math.pow(chunk, 1/2)
                // else chunk = Math.pow(chunk/value, 1/value)*value
                // Paint loop
                for (let j = 0; j < chunk; j++) {
                    ctx.beginPath()
                    ctx.fillStyle = `hsl(${i/max*30}deg,100%,50%)`
                    ctx.arc(
                        fx(i), 
                        i, 
                        i/max*50, 
                        0, Math.PI*2
                    )
                    ctx.fill()
                    i++
                    if (i > val) break
                }
                if (i < val) requestAnimationFrame(discover)
            }
            window.addEventListener('scroll', function _ () {
                const rect = roadmap.getBoundingClientRect()
                if (window.pageYOffset >= rect.top + (rect.bottom - rect.top)/2) {
                    discover()
                    window.removeEventListener('scroll', _)
                }
            })
            // Text
            for (const [i, el] of Object.entries(elements)) {
                let y = (+i+1)*gap
                let size = '30px'
                if (isMobile) size = '25px'
                if (window.innerWidth <= 800) size = '14px'
                let text = document.createElement('span')
                text.className = 'text'
                text.innerHTML = el.text
                text.style.position = 'absolute'
                text.style.top = `${y / scale}px`
                // Position
                if (i % 2 == 1)
                    text.style.left = (isMobile) ? '25px' : '0'
                else
                    text.style.right = (isMobile) ? '25px' : '0'
                // Color
                if (+i+1 > value) {
                    text.style.color = '#888'
                    for (const child of text.children) {
                        if (child.tagName == 'OBJECT') {
                            child.addEventListener('load', function() {
                                const doc = this.getSVGDocument()
                                doc.querySelector('svg').style.fill = '#888'
                            })
                        }
                    }
                }
                else {
                    text.style.color = 'hsl(15deg,100%,50%)'
                    for (const child of text.children) {
                        if (child.tagName == 'OBJECT') {
                            child.addEventListener('load', function() {
                                const doc = this.getSVGDocument()
                                doc.querySelector('svg').style.fill = 'hsl(15deg,100%,50%)'
                            })
                        }
                    }
                }
                text.style.fontSize = size
                if (isMobile) text.style.maxWidth = `${window.innerWidth/3}px`
                else text.style.maxWidth = `${window.innerWidth/6}px`
                text.style.transform = 'translate(0,-50%)'
                roadmap.appendChild(text)

            }
            // Marker
            for(let i = 1; i <= elements.length; i++) {
                let size = i*gap/max*50
                
                ctx2.beginPath()
                ctx2.arc(
                    fx(+i*gap), 
                    i*gap, 
                    size * 1.2, 
                    0, Math.PI*2
                )
                // Color
                if (+i > value) {
                    ctx2.fillStyle = '#888'
                }
                else {
                    ctx2.fillStyle = '#fff'
                    ctx2.strokeStyle = 'hsl(15deg,100%,50%)'
                    ctx2.lineWidth = 5
                    ctx2.stroke()
                }
                ctx2.fill()
                function blitLogo() {
                    let factor = 1.7
                    let newsize = size * factor
                    if (+i > value)
                        ctx2.filter = 'grayscale(100%)'
                    else
                        ctx2.filter = 'grayscale(0%)'
                    ctx2.drawImage(logo, fx(+i*gap)-newsize/2, i*gap-newsize/2, newsize, newsize)                      
                }
                logo.addEventListener('load', blitLogo)
                blitLogo()
            }
        }
        draw()
        let time = null
        window.addEventListener('resize', e => {
            clearTimeout(time)
            time = setTimeout(() => {
                for (const item of [...roadmap.children]) {
                    if (item.tagName == 'CANVAS') item.remove()
                    if (item.className == 'text') item.remove()
                }
                draw()
            }, 500)
        })
    }
}

main()
})()